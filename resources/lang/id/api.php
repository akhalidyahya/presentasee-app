<?php

return [
    'data_saved' => ':name berhasil disimpan',
    'data_deleted' => ':name dihapus!',
    'data_not_processed' => ':name gagal diproses!',
    'not_found' => ':name tidak dapat ditemukan!',
];
