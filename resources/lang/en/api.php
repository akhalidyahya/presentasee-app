<?php

return [
    'data_saved' => ':name saved!',
    'data_deleted' => ':name deleted!',
    'data_not_processed' => ':name not processed!',
    'not_found' => ':name not found!',
];
