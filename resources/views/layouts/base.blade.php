<!DOCTYPE html>
<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="../assets/" data-template="vertical-menu-template-free">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>@yield('title') | Presentasee</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon/about-image.3b353a9a.png') }}" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
        href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
        rel="stylesheet" />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}"
        class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/sweetalert/sweetalert.css') }}" />

    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/apex-charts/apex-charts.css') }}" />

    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />

    <!-- Page CSS -->

    @stack('customCSS')

    <!-- Helpers -->
    <script src="{{ asset('assets/vendor/js/helpers.js') }}"></script>

    <script src="{{ asset('assets/js/config.js') }}"></script>
</head>

<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            <!-- Menu -->

            <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
                <div class="app-brand demo">
                    <a href="#" class="app-brand-link">
                        <span class="app-brand-logo demo">
                            <img src="{{asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="35px">
                        </span>
                        <span class="app-brand-text demo menu-text fw-bolder ms-2">Presentasee</span>
                    </a>

                    <a href="javascript:void(0);"
                        class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                        <i class="bx bx-chevron-left bx-sm align-middle"></i>
                    </a>
                </div>

                <div class="menu-inner-shadow"></div>

                @include('layouts.sidebar')
            </aside>
            <!-- / Menu -->

            <!-- Layout container -->
            <div class="layout-page">
                <!-- Navbar -->
                @include('layouts.navbar')
                <!-- / Navbar -->

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    <!-- Content -->
                    @yield('content')
                    <!-- / Content -->

                    <!-- Basic footer -->
                    <section id="basic-footer">
                        <footer class="footer bg-light">
                            <div
                                class="container-fluid d-flex flex-md-row flex-column justify-content-between align-items-md-center gap-1 container-p-x py-3">
                                <div>
                                    <a href="https://presentasee.com" target="_blank"
                                        class="footer-text fw-bolder">Presentasee</a>
                                    ©
                                </div>
                                <div>
                                    <a href="https://themeselection.com/license/" class="footer-link me-4"
                                        target="_blank">License</a>
                                    <a href="javascript:void(0)" class="footer-link me-4">Help</a>
                                    <a href="javascript:void(0)" class="footer-link me-4">Contact</a>
                                    <a href="javascript:void(0)" class="footer-link">Terms &amp; Conditions</a>
                                </div>
                            </div>
                        </footer>
                    </section>
                    <!--/ Basic footer -->

                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->

    <div class="buy-now">
        <a href="#" target="_blank" class="btn btn-danger btn-buy-now">Beralih ke Pro</a>
    </div>
    @include('pages.interactive-floor.part.modal-create-project')

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('assets/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>

    <script src="{{ asset('assets/vendor/js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="{{ asset('assets/vendor/libs/apex-charts/apexcharts.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Main JS -->
    <script src="{{ asset('assets/js/main.js') }}"></script>

    <!-- Page JS -->
    <script src="{{ asset('assets/js/dashboards-analytics.js') }}"></script>
    <script src="{{ asset('assets/js/ui-toasts.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("body").tooltip({ selector: '[data-bs-toggle=tooltip]' });
        });
        
        function renderError(response) {
            for(key in response) {
                $('#error-'+key).text(response[key]);
            }
        }

        function resetError() {
            $('.error').text('');
        }

        function addProject() {
            $('#modalCreateProject .modal-title').text('Detail Event');
            $('#modalCreateProject form')[0].reset();
            $('#modalCreateProject form [name="projectId"]').val('');
            $('#modalCreateProject').modal('show');
        }

        function saveProject() {
            resetError();
            $.ajax({
                url: "{{ route('interactiveFloor.project.saveProject') }}",
                type: "POST",
                data: new FormData($('#formProject')[0]),
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.success) {
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            icon: 'success',
                            timer: '3000'
                        });
                        if($('#modalCreateProject [name="projectId"]').value) {
                            $('.event-title').text(response.data.title);
                        } else {
                            // redirect to detail page if create new
                            let urlRedirect = "{{route('interactiveFloor.view.project.detail',['projectId'=>':id'])}}";
                            urlRedirect = urlRedirect.replace(':id',response.data.id);
                            location.href = urlRedirect;
                        }
                        $('#modalCreateProject').modal('hide');
                    } else {
                        swal({
                            title: 'Gagal',
                            text: response.message,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    let title = 'Server Error';
                    let text = errorThrown;
                    if(jqXHR.responseJSON.errors) {
                        title = 'Gagal';
                        text = 'Periksa data kembali';
                    }
                    swal({
                        title: title,
                        text: text,
                        icon: 'error',
                        timer: '3000'
                    }).then(()=>{
                        if(jqXHR.responseJSON.errors) {
                            renderError(jqXHR.responseJSON.errors)
                        }
                    });
                }
            });
        }
    </script>

    @stack('customJS')

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
</body>

</html>
