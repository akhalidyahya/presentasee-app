<ul class="menu-inner py-1">
    <!-- Dashboard -->
    <li class="menu-item @if($sidebar == 'dashboard') active @endif">
        <a href="{{route('dashboard.index')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-home-circle"></i>
            <div data-i18n="Analytics">Dasbor</div>
        </a>
    </li>

    <li class="menu-header small text-uppercase">
        <span class="menu-header-text">Interactive Floor Plan</span>
    </li>
    <li class="menu-item">
        <a href="javascript:;" onclick="addProject()" class="menu-link">
            <i class="menu-icon tf-icons bx bx-plus"></i>
            <div data-i18n="Basic">Buat baru</div>
        </a>
    </li>
    <li class="menu-item @if($sidebar == 'projectList') active @endif">
        <a href="{{route('interactiveFloor.view.project.list')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-collection"></i>
            <div data-i18n="Basic">Event Saya</div>
        </a>
    </li>
    
    <li class="menu-header small text-uppercase"><span class="menu-header-text">Interactive Slide</span></li>
    <li class="menu-item">
        <a href="javascript:;" class="menu-link">
            <i class="menu-icon tf-icons bx bx-collection"></i>
            <div data-i18n="Basic">Coming Soon..</div>
        </a>
    </li>

    {{-- <li class="menu-header small text-uppercase"><span class="menu-header-text">Akun</span></li>
    <li class="menu-item">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="menu-icon tf-icons bx bx-dock-top"></i>
            <div data-i18n="Account Settings">pengaturan</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item">
                <a href="pages-account-settings-account.html" class="menu-link">
                    <div data-i18n="Account">Account</div>
                </a>
            </li>
            <li class="menu-item">
                <a href="pages-account-settings-notifications.html" class="menu-link">
                    <div data-i18n="Notifications">Notifications</div>
                </a>
            </li>
            <li class="menu-item">
                <a href="pages-account-settings-connections.html" class="menu-link">
                    <div data-i18n="Connections">Connections</div>
                </a>
            </li>
        </ul>
    </li>
    <li class="menu-item">
        <a href="#" class="menu-link">
            <i class="menu-icon tf-icons bx bx-power-off"></i>
            <div data-i18n="Basic">Keluar</div>
        </a>
    </li> --}}
</ul>