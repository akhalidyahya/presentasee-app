@extends('layouts.base')
@section('title', 'Dasbor')
@push('customCSS')
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4">
            <span class="">Dasbor</span> 
        </h4>
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-9">
                            <div class="card-body">
                                <h5 class="card-title text-primary">Selamat datang {{\Auth::user()->name}}! 🎉</h5>
                                <p class="mb-4">
                                    Solusi presentasi di dalam ruang maupun di luar ruangan yang lebih interaktif, komunikatif, terhubung dan terukur <span class="fw-bold">#TanpaBatasJarak</span>.
                                </p>

                                {{-- <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a> --}}
                            </div>
                        </div>
                        <div class="col-sm-3 text-center text-sm-left">
                            <div class="card-body pb-0 px-0 px-md-4">
                                <img src="{{ asset('assets/img/illustrations/man-with-laptop-light.png') }}" height="140"
                                    alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                    data-app-light-img="illustrations/man-with-laptop-light.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('customJS')
    @endpush
@endsection
