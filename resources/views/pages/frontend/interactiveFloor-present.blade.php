@extends('layouts.frontend.base')
@section('title', $event->title)
@push('customCSS')
@endpush
@section('content')
    <div class="container-xxl">
        <div class="authentication-wrapper authentication-basic py-4">
            <div class="authentication-inner present">
                <!-- Register -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center mb-2">
                                <img src="{{ !empty(@$event->logo)? @url(@$event->logo) : asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="100px">
                            </div>
                        </div>
                        <h2 class="text-center">{{@$event->title}}</h2>
                        <div class="row">
                            <div class="col-md-12">
                                @if(!$isIframe)
                                <div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-2 mb-4">
                                        <select class="form-select" id="floor" name="floor" aria-label="Default select example" onchange="redirect()">
                                            @foreach ($event->interactiveFloors as $interactiveFloor)
                                            <option value="{{$interactiveFloor->id}}"
                                                    @if($interactiveFloor->id == $floor->id) selected @endif >{{$interactiveFloor->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div id="mainImage" style="position: relative;">
                                    <div data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="" 
                                        id="infoBoxBtn-default" info-box-id="0" class="zindex-5 info-box d-none" onclick="viewInfoBox(0)"><i class="bx bxs-info-circle bx-xs infobox-icon" style="transform: translateY(-5px);"></i></div>
                                    <img class="main-image" src="{{@url(@$floor->main_image)}}" alt="" srcset="" width="{{@$floor->width}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Register -->
            </div>
        </div>
    </div>
    <div class="powered-by">
        <div class="btn-powered-by">
            <small>Powered by:</small>
            <a href="{{route('home.index')}}" class="app-brand-link gap-2">
                <span class="app-brand-logo demo">
                    <img src="{{asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="30px">
                </span>
                <span class="app-brand-text present demo text-body fw-bolder">Presentasee</span>
            </a>
        </div>
    </div>
    @include('pages.frontend.part.infoBox')
    @include('pages.frontend.part.infoBoxSold')
    @include('pages.frontend.part.guestBook')
    @push('customJS')
    <script>
        // Guest book if any
        $(document).ready(()=>{
            @if(!session()->has('guestName') && !$isIframe)
            $('#modalGuestBook').modal('show');
            @endif
        })

        // Global variable
        var is_xs = window.innerWidth < 768;

        if(is_xs) {
            $('#mainImage').css('width','100%');
        } else {
            $('#mainImage').css('width','{{@$floor->width}}px');
        }

        function saveGuest() {
            $.ajax({
                url: "{{ route('guestBook',['projectId'=>$event->id]) }}",
                type: "POST",
                data: {
                    '_token' : '{{csrf_token()}}',
                    'name' : $('#modalGuestBook form [name="name"]').val(),
                },
                success: function(response) {
                    if (response.success) {
                        $('#modalGuestBook').modal('hide');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }

        // initiate
        let interactiveFloor = {!!json_encode($floor)!!};

        interactiveFloor.info_box.forEach((infoBox,index)=>{
            createElementInfoBox(infoBox.id,infoBox,interactiveFloor.width,interactiveFloor.height)
        });

        function createElementInfoBox(key,content,width,height){
            let infoBox = $('#infoBoxBtn-default').clone();
            infoBox.prop('id', 'infoBoxBtn-' + key);
            infoBox.prop('info-box-id', content.id);
            infoBox.attr('onclick', 'viewInfoBox(' + content.id + ')');
            infoBox.attr('data-bs-original-title', content.title);
            if(content.is_sold) {
                infoBox.attr('data-bs-original-title', content.tenant.name);
                infoBox.css('color','#ff3e1d');
            } else {
                infoBox.css('color','#03c3ec');
            }

            let canvas = document.getElementById('mainImage');
            let current_width = canvas.offsetWidth;
            let current_height = canvas.offsetHeight;
            let x = ((content.pos_x / width) * current_width);
            let y = ((content.pos_y / height) * current_height) - 5;
            // if(is_xs) {
            // }
            x = content.pos_x;
            y = content.pos_y - 5;

            infoBox.css({left: x + 'px', top: y + 'px',});
            infoBox.removeClass('d-none');

            infoBox.appendTo('#mainImage');
        }

        function viewInfoBox(id) {
            let data = [];
            interactiveFloor.info_box.forEach((infoBox,index)=>{
                if(infoBox.id == id) {
                    data = infoBox;
                }
            });
            if(data) {
                // Handle modal
                if(data.is_sold) {
                    data = data.tenant;
                    $('#modalInfoBoxSold .modal-title').text(data.name);
                    $('#modalInfoBoxSold #description').text(data.description);
                    $('#modalInfoBoxSold #imgTenant').attr('src',"{{url('')}}/"+data.logo);
                    $('#modalInfoBoxSold #externalLink').attr('href',data.external_link);
                    if(data.external_link) {
                        $('#modalInfoBoxSold #externalLink').removeClass('d-none');
                    } else {
                        $('#modalInfoBoxSold #externalLink').addClass('d-none');
                    }
                    $('#modalInfoBoxSold').modal('show');
                } else {
                    $('#modalInfoBox .modal-title').text(data.title);
                    $('#modalInfoBox #size').text(`[${data.size || ''}]`);
                    $('#modalInfoBox #price').text(data.price);
                    $('#modalInfoBox #description').text(data.description);
                    $('#modalInfoBox #imgInfoBox').attr('src',"{{url('')}}/"+data.images);
                    $('#modalInfoBox #externalLink').attr('href',data.external_link);
                    if(data.external_link) {
                        $('#modalInfoBox #externalLink').removeClass('d-none');
                    } else {
                        $('#modalInfoBox #externalLink').addClass('d-none');
                    }
                    $('#modalInfoBox').modal('show');
                }
            }
        }

        function redirect() {
            let floorId = $('[name="floor"]').val();
            let url = "{{route('interactiveFloor.present',['eventCode'=>$event->code,'floorId'=>':id'])}}";
            url = url.replace(':id', floorId);
            location.href = url;
        }

        document.addEventListener('DOMContentLoaded', function () {
            new PerfectScrollbar(document.getElementById('mainImage'), {
                wheelPropagation: false
            });
        })

    </script>
    @endpush
@endsection
