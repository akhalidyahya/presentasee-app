@extends('layouts.frontend.base')
@section('title', $event->title)
@push('customCSS')
@endpush
@section('content')
    <div class="container-xxl">
        <div class="authentication-wrapper authentication-basic container-p-y">
            <div class="authentication-inner">
                <!-- Register -->
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="d-flex align-items-center justify-content-center mb-2">
                                <img src="{{ !empty(@$event->logo)? @url(@$event->logo) : asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="100px">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-center">{{$event->title}}</h2>
                                <p class="text-justify">
                                    {{$event->description}}
                                </p>
                                <div class="text-center">
                                    <a href="{{route('interactiveFloor.present',['eventCode'=>$event->code,'floorId'=>@$event->interactiveFloors[0]->id])}}" class="btn btn-primary">Mulai <i class="bx bx-chevron-right-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Register -->
            </div>
        </div>
    </div>
    <div class="powered-by">
        <div class="btn-powered-by">
            <small>Powered by:</small>
            <a href="{{route('home.index')}}" class="app-brand-link gap-2">
                <span class="app-brand-logo demo">
                    <img src="{{asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="30px">
                </span>
                <span class="app-brand-text present demo text-body fw-bolder">Presentasee</span>
            </a>
        </div>
    </div>
    @include('pages.frontend.part.guestBook')
    @push('customJS')
    <script>
        // guest book if any
        $(document).ready(()=>{
            @if(!session()->has('guestName'))
            $('#modalGuestBook').modal('show');
            @endif
        })

        function saveGuest() {
            $.ajax({
                url: "{{ route('guestBook',['projectId'=>$event->id]) }}",
                type: "POST",
                data: {
                    '_token' : '{{csrf_token()}}',
                    'name' : $('#modalGuestBook form [name="name"]').val(),
                },
                success: function(response) {
                    if (response.success) {
                        $('#modalGuestBook').modal('hide');
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    </script>
    @endpush
@endsection
