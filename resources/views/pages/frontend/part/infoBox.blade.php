<!-- Modal -->
<div class="modal fade" id="modalInfoBox" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{-- <div class="mb-3">
                            <h6 id="title"></h6>
                        </div> --}}
                        <div class="mb-3">
                            <div id="size"></div>
                            <div id="price"></div>
                            <div id="description"></div>
                        </div>
                        <div class="mb-3">
                            <img id="imgInfoBox" src="" alt="" srcset="" width="100%">
                        </div>
                        <div class="mb-3">
                            <a target="_blank" class="btn btn-primary" id="externalLink" href="">Kunjungi kami</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-danger float-start" onclick="deleteInfoBox()">Hapus</button> --}}
                    {{-- <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button> --}}
                    {{-- <button type="button" class="btn btn-primary" onclick="saveInfoBox()">Simpan</button> --}}
                </div>
            </div>
        </form>
    </div>
</div>
