<!-- Modal -->
<div class="modal fade" id="modalGuestBook" tabindex="-1" aria-hidden="true" data-bs-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <form method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Isi buku tamu dulu yuk! 😁</h5>
                    {{-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> --}}
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="mb-3 text-center">
                            <label for="nama" class="form-label">Namamu siapa?</label>
                            <input type="text" id="nama" name="name" class="form-control"
                                placeholder="John Doe" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="saveGuest()">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
