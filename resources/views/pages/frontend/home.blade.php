@extends('layouts.frontend.base')
@section('title', 'Beranda')
@push('customCSS')
@endpush
@section('content')
    <div class="container-xxl">
        <div class="authentication-wrapper authentication-basic container-p-y">
            <div class="authentication-inner">
                <!-- Register -->
                <div class="card">
                    <div class="card-body">
                        <!-- Logo -->
                        <div class="app-brand justify-content-center">
                            <a href="{{route('home.index')}}" class="app-brand-link gap-2">
                                <span class="app-brand-logo demo">
                                    <img src="{{asset('assets/img/favicon/about-image.3b353a9a.png')}}" alt="" srcset="" width="50px">
                                </span>
                                <span class="app-brand-text demo text-body fw-bolder">Presentasee</span>
                            </a>
                        </div>
                        <!-- /Logo -->

                        @if ($errors->any())
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    {{ $error }}
                                    <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                </div>
                            @endforeach
                        @endif

                        @if (session('message'))
                            <div class="alert alert-{{ @session('message')['status'] }} alert-dismissible" role="alert">
                                {{ @session('message')['message'] }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @endif

                        <form id="formAuthentication" class="mb-5" action="{{ route('join') }}" method="GET">
                            <div class="mb-3">
                                <input type="text" class="form-control text-center" id="code" name="code"
                                    placeholder="Kode Unik" autofocus />
                            </div>
                            <div class="mb-3">
                                <button class="btn btn-primary d-grid w-100" type="submit">Masuk</button>
                            </div>
                        </form>

                        <p class="text-center mb-0 small">
                            <span>Mau buat presentasi?</span>
                            <a href="{{ route('register.index') }}">
                                <span>Join yuk!</span>
                            </a>
                        </p>
                        <p class="text-center small">
                            <span>Sudah punya akun?</span>
                            <a href="{{ route('login.index') }}">
                                <span>Login disini!</span>
                            </a>
                        </p>
                    </div>
                </div>
                <!-- /Register -->
            </div>
        </div>
    </div>
    @push('customJS')
    @endpush
@endsection
