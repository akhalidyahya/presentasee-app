@extends('layouts.base')
@section('title', 'Interactive Floor')
@push('customCSS')
    <!-- Datatable css -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/responsive.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.checkboxes.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/buttons.bootstrap5.css') }}">
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4">
            <span class="muted fw-light">Interactive Floor/</span> Event Saya
        </h4>
        <div class="row">
            <div class="col-lg-4 mb-4 order-0">
                <a href="#" class="cutom-section-link" onclick="addProject()">
                    <div class="card border-only">
                        <div class="card-body">
                            <h5 class="card-title text-center m-0"><span class="tf-icons bx bx-plus-circle"></span> Buat
                                Baru</h5>
                        </div>
                    </div>
                </a>
            </div>
            @forelse($projectData as $project)
                <div class="col-lg-4 mb-4 order-0">
                    <div class="card cutom-section-link">
                        <div class="delete-project">
                            <a href="{{ route('interactiveFloor.view.project.detail', $project->id) }}"><i
                                class="bx bx-pencil bx-xs text-info"
                                data-bs-toggle="tooltip" 
                                data-bs-offset="0,4" 
                                data-bs-placement="top" 
                                data-bs-html="true" title="" 
                                data-bs-original-title="Edit"
                                ></i></a>
                            <a href="{{route('interactiveFloor.present',['eventCode'=>$project->code])}}" target="_blank"><i
                                class="bx bx-share-alt bx-xs text-primary"
                                data-bs-toggle="tooltip" 
                                data-bs-offset="0,4" 
                                data-bs-placement="top" 
                                data-bs-html="true" title="" 
                                data-bs-original-title="Tampilkan"
                                ></i></a>
                            <a href="#" onclick="deleteProject({{ $project->id }})"><i
                                class="bx bx-trash bx-xs text-danger"
                                data-bs-toggle="tooltip" 
                                data-bs-offset="0,4" 
                                data-bs-placement="top" 
                                data-bs-html="true" title="" 
                                data-bs-original-title="Hapus"></i></a>
                        </div>
                        <a href="{{ route('interactiveFloor.view.project.detail', $project->id) }}"
                            class="">
                            <div class="card-body">
                                <div class="text-center">
                                    <img src="{{@url($project->logo)}}" alt="" srcset="" width="100px">
                                </div>
                                <h5 class="card-title text-center m-0">{{ $project->title }}</h5>
                            </div>
                        </a>
                    </div>
                </div>
            @empty
                <div class="col-lg-4 mb-4 order-0">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">Belum ada event. <br> Yuk buat event pertamamu! 😀</div>
                        </div>
                    </div>
                </div>
            @endforelse
        </div>
    </div>
    @push('customJS')
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.responsive.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/responsive.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.checkboxes.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-buttons.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.html5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.print.js') }}"></script>
        {{-- <script src="{{ asset('assets/js/docs-tables-datatables.js') }}"></script> --}}
        <script>
            $(function() {

                'use strict';

                var dt_basic_table = $('.datatables-basic');
                dt_basic_table.DataTable({
                    'processing': true,
                    'serverSide': true,
                    'language': {
                        processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw" style="color:#07a7e1"></i><span class="sr-only">Loading...</span> '
                    },
                    'ajax': {
                        url: "{{ asset('assets/json/table-datatable.json') }}",
                    },
                    'dataType': 'json',
                    'searching': true,
                    'paging': true,
                    'lengthChange': true,
                    'columns': [{
                            data: 'id',
                            name: 'id'
                        },
                        {
                            data: 'full_name',
                            name: 'full_name'
                        },
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'start_date',
                            name: 'start_date'
                        },
                        {
                            data: 'salary',
                            name: 'salary'
                        },
                        {
                            data: 'status',
                            name: 'status'
                        },
                        // { data: '', name: ''},
                    ],
                    'info': true,
                    'autoWidth': false
                });
            });

            function share(id) {

            }

            function deleteProject(id) {
                swal({
                    title: "Yakin Hapus Event?",
                    text: "Data akan dihapus permanen",
                    icon: "warning",
                    buttons: {
                        cancel: true,
                        confirm: {
                            text: 'Hapus!',
                            closeModal: false,
                        },
                    },
                }).then((process) => {
                    if (process) {
                        let url = "{{ route('interactiveFloor.project.delete', ['projectId' => ':id']) }}";
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            data: {
                                '_token': "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                swal({
                                    title: 'Berhasil',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '3000'
                                }).then(() => {
                                    location.reload();
                                });
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                swal({
                                    title: 'System Error',
                                    text: errorThrown,
                                    icon: 'error',
                                    timer: '3000'
                                });
                            }
                        });
                    }
                })
            }
        </script>
    @endpush
@endsection
