@extends('layouts.base')
@section('title', 'Interactive Floor')
@push('customCSS')
    <!-- Datatable css -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/responsive.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.checkboxes.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/buttons.bootstrap5.css') }}">
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-2">
            <span class="muted fw-light">Interactive Floor/</span>
            <span class="muted fw-light"><a class="text-secondary text-decoration-underline"
                    href="{{ route('interactiveFloor.view.project.list') }}">Event Saya</a>/</span>
            <span class="event-title">{{ $projectData->title }}</span> <a href="javascript:;"
                onclick="editProject({{ $projectData->id }})"><span class="tf-icons bx bx-pencil"></span></a>
        </h4>
        <h5 class="mb-2">
            kode unik: {{ $projectData->code }}
            <a href="{{ route('interactiveFloor.present', ['eventCode' => $projectData->code]) }}" target="_blank"><i
                    class="bx bx-share-alt bx-xs text-primary" data-bs-toggle="tooltip" data-bs-offset="0,4"
                    data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title="Tampilkan"></i></a>
        </h5>
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-9">
                                <h5 class="card-title">
                                    <a href="{{ route('interactiveFloor.view.project.list') }}" class="text-secondary"><i
                                            class="bx bx-chevron-left bx-sm"></i></a>
                                    Daftar Interactive Floor (IF) <span
                                        class="event-title">{{ $projectData->title }}</span>
                                </h5>
                            </div>
                            <div class="col-md-3">
                                <div class="form-check form-switch mb-2 text-right">
                                    <input class="form-check-input" type="checkbox" id="tayang" name="tayang"
                                        value="1" onchange="setStatus()"
                                        @if ($projectData->status == \App\Util\Constant::PUBLISH_STATUS_PUBLISHED) checked @endif />
                                    <label class="form-check-label" for="tayang"><strong>Tayangkan ke
                                            publik</strong></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-end">
                                    <a href="{{ route('interactiveFloor.create', ['projectId' => $projectData->id]) }}"
                                        class="btn btn-icon btn-primary">
                                        <span class="bx bx-plus"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-datatable table-responsive text-nowrap pt-0">
                            <table class="datatables-basic table border-top">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama IF</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('pages.interactive-floor.part.modal-share-floor')
    @push('customJS')
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.responsive.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/responsive.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.checkboxes.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-buttons.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.html5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.print.js') }}"></script>
        {{-- <script src="{{ asset('assets/js/docs-tables-datatables.js') }}"></script> --}}

        <script>
            let datatableUrl = "{{ route('interactiveFloor.getDataTable', ['projectId' => $projectData->id]) }}";

            var dt_basic_table = $('.datatables-basic');
            dt_basic_table.DataTable({
                'processing': true,
                'serverSide': true,
                'language': {
                    processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw" style="color:#07a7e1"></i><span class="sr-only">Loading...</span> '
                },
                'ajax': {
                    url: datatableUrl,
                },
                'dataType': 'json',
                'searching': true,
                'paging': true,
                'lengthChange': true,
                'columns': [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                'info': true,
                'autoWidth': false
            });


            function editProject(id) {
                let url = "{{ route('interactiveFloor.project.getProject', ['projectId' => ':id']) }}";
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(response) {
                        $('#modalCreateProject .modal-title').text('Detail Event');
                        $('#modalCreateProject form')[0].reset();
                        $('#modalCreateProject form [name="projectId"]').val(response.id);
                        $('#modalCreateProject form img').attr('src','{{url('')}}/'+response.logo);
                        $('#modalCreateProject form [name="title"]').val(response.title);
                        $('#modalCreateProject form [name="description"]').val(response.description);
                        $('#modalCreateProject').modal('show');
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }

            function setStatus() {
                console.log($('[name="tayang"]').prop("checked"));
                let status = '';
                $('[name="tayang"]').prop("checked") ? status = '{{ \App\Util\Constant::PUBLISH_STATUS_PUBLISHED }}' : status =
                    '{{ \App\Util\Constant::PUBLISH_STATUS_UNPUBLISH }}';
                let url =
                    "{{ route('interactiveFloor.project.setStatus', ['projectId' => $projectData->id, 'status' => ':status']) }}";
                url = url.replace(':status', status);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function(response) {
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            icon: 'success',
                            timer: '3000'
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }

            function deleteFloor(id) {
                swal({
                    title: "Yakin Hapus Interactive Floor?",
                    text: "Data akan dihapus permanen",
                    icon: "warning",
                    buttons: {
                        cancel: true,
                        confirm: {
                            text: 'Hapus!',
                            closeModal: false,
                        },
                    },
                }).then((process) => {
                    if (process) {
                        let url =
                            "{{ route('interactiveFloor.delete', ['projectId' => $projectData->id, 'interactiveFloorId' => ':id']) }}";
                        url = url.replace(':id', id);
                        $.ajax({
                            url: url,
                            type: "DELETE",
                            data: {
                                '_token': "{{ csrf_token() }}"
                            },
                            success: function(response) {
                                swal({
                                    title: 'Berhasil',
                                    text: response.message,
                                    icon: 'success',
                                    timer: '3000'
                                }).then(() => {
                                    location.reload()
                                });
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                swal({
                                    title: 'System Error',
                                    text: errorThrown,
                                    icon: 'error',
                                    timer: '3000'
                                });
                            }
                        });
                    }
                })

            }

            function shareFloor(title,link,iframe) {
                $('#modalShareFloor .modal-title').text(title);
                $('#modalShareFloor [name="share_link"]').val(link);
                let iFrameHTML = '<iframe src="'+iframe+'" frameborder="0" width="" height=""></iframe>'
                $('#modalShareFloor [name="share_iframe"]').val(iFrameHTML);
                $('#modalShareFloor').modal('show');
            }
        </script>
    @endpush
@endsection
