@extends('layouts.base')
@section('title', 'Floor Plan')
@push('customCSS')
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4">
            <span class="muted fw-light">Interactive Floor/</span>
            <span class="muted fw-light"><a class="text-secondary text-decoration-underline"
                    href="{{ route('interactiveFloor.view.project.list') }}">Event Saya</a>/</span>
            <span class="muted fw-light"><a class="text-secondary text-decoration-underline"
                    href="{{ route('interactiveFloor.view.project.detail', ['projectId' => $projectId]) }}">{{ @$interactiveFloorData->project->title ?? @$project->title }}</a>/</span>
            {{ @$interactiveFloorData->title }}
        </h4>
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h5 class="mb-0">Data Lantai</h5>
                        {{-- <small class="text-muted float-end">Default label</small> --}}
                    </div>
                    <div class="card-body">
                        <form id="mainForm" method="POST" action="" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="interactiveFloorId" value="{{@$interactiveFloorData->id}}">
                            <div class="mb-3">
                                <label class="form-label" for="title-form">Nama Lantai</label>
                                <input type="text" name="title" class="form-control" id="title-form"
                                    placeholder="Lantai 1" value="{{ @$interactiveFloorData->title }}" />
                                <div id="error-title" class="error text-danger"></div>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="description-form">Deskripsi</label>
                                <textarea id="description-form" class="form-control" name="description" placeholder="Lantai 1 merupakan...">{{ @$interactiveFloorData->description }}</textarea>
                                <div id="error-description" class="error text-danger"></div>
                            </div>
                            <div class="mb-3">
                                <label for="formFile" class="form-label">Gambar lantai</label>
                                <input class="form-control" type="file" id="formFile" name="main_image" />
                                <div id="mainImage" class="content-wrapper" style="position: relative;" >
                                    <div data-bs-toggle="tooltip" data-bs-offset="0,4" data-bs-placement="top" data-bs-html="true" title="" data-bs-original-title=""
                                    id="infoBoxBtn-default" info-box-id="0" class="zindex-5 info-box d-none" onclick="viewInfoBox(0)"><i class="bx bxs-info-circle bx-xs infobox-icon" style="transform: translateY(-5px);"></i></div>
                                    <img class="main-image mt-2" src="@if(!empty($interactiveFloorData->main_image)) {{@url(@$interactiveFloorData->main_image)}} @endif" alt="" srcset="" width="100%">
                                </div>
                                <div id="error-main_image" class="error text-danger"></div>
                            </div>
                            <button type="button" class="btn btn-primary" onclick="save()">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('pages.interactive-floor.part.modal-create-infobox')
    @push('customJS')
    <script>
        let infoBoxList = [];
        @if(!empty($interactiveFloorData->infoBox))
        @foreach($interactiveFloorData->infoBox as $infoBox)
        // infoBoxList.push({
        //     id: Number('{{$infoBox->id}}'),
        //     x: Number('{{$infoBox->pos_x}}'),
        //     y: Number('{{$infoBox->pos_y}}'),
        //     title : '{{$infoBox->title}}'
        // });
        infoBoxList.push(JSON.parse('{!! json_encode($infoBox) !!}'));
        @endforeach
        @endif

        infoBoxList.forEach((el,index)=>{
            createElementInfoBox(index,el)
        });

        // Add infobox
        @if(!empty(@$interactiveFloorData->main_image))
        $('#mainImage').on("click",function(event) {
            // handle click info box not trigger another add infobox
            let infobox = [].slice.call(document.getElementsByClassName('infobox-icon'));
            if(infobox.indexOf(event.target) != -1) return;

            let canvas = document.getElementById('mainImage');
            let elWidth = canvas.offsetWidth;
            let elHeight = canvas.offsetHeight;
            var rect = canvas.getBoundingClientRect();
            var x = event.clientX - rect.left - 5;
            var y = event.clientY - rect.top - 5;
            
            setInfoBox(x,y,elWidth,elHeight);
        })
        @endif

        function save() {
            resetError();
            let url = "{{route('interactiveFloor.save',['projectId'=>$projectId,'interactiveFloorId'=>@$interactiveFloorData->id])}}";
            $.ajax({
                url: url,
                type: "POST",
                contentType: false,
                processData: false,
                data: formData = new FormData($('#mainForm')[0]),
                success: function(response) {
                    if (response.success) {
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            icon: 'success',
                            timer: '3000'
                        }).then(()=> {
                            if(response.data) {
                                let url = "{{ route('interactiveFloor.detail',['projectId'=>$projectId,'interactiveFloorId'=>':id']) }}";
                                url = url.replace(':id',response.data.id);
                                location.href=url;
                            } else {
                                location.reload();
                            }
                        });
                        
                    } else {
                        swal({
                            title: 'Gagal',
                            text: response.message,
                            icon: 'error',
                            timer: '3000'
                        }).then(()=>{
                            if(response.errors) {
                                renderError(response.errors)
                            }
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    let title = 'Server Error';
                    let text = errorThrown;
                    if(jqXHR.responseJSON.errors) {
                        title = 'Gagal';
                        text = 'Periksa data kembali';
                    }
                    swal({
                        title: title,
                        text: text,
                        icon: 'error',
                        timer: '3000'
                    }).then(()=>{
                        if(jqXHR.responseJSON.errors) {
                            renderError(jqXHR.responseJSON.errors)
                        }
                    });
                }
            });
        }

        function setInfoBox(x,y,width=0,height=0) {
            $('#formInfoBox [name="id"]').val(0);
            $('#formInfoBox [name="title"]').val('');
            $('#formInfoBox [name="description"]').val('');
            $('#formInfoBox [name="pos_x"]').val(x);
            $('#formInfoBox [name="pos_y"]').val(y);
            $('#formInfoBox [name="size"]').val('');
            $('#formInfoBox [name="price"]').val('');
            $('#formInfoBox [name="external_link"]').val('');
            $('#formInfoBox [name="tenant_id"]').val(0);
            $('#modalCreateInfoBox form [name="sold"]').prop("checked",false);
            
            if(width && height) {
                $('#formInfoBox [name="mainImgWidth"]').val(width);
                $('#formInfoBox [name="mainImgHeight"]').val(height);
            }
            $.ajax({
                url: "{{ route('interactiveFloor.save.infoBox',['projectId'=>$projectId,'interactiveFloorId'=>@$interactiveFloorData->id]) }}",
                type: "POST",
                contentType: false,
                processData: false,
                data: new FormData($('#formInfoBox')[0]),
                success: function(response) {
                    if (response.success) {
                        infoBoxList.push(response.data);
                        infoBoxList.forEach((el,index)=>{
                            createElementInfoBox(index,el)
                        });
                        viewInfoBox(response.data.id, response.data);
                        $('#modalCreateInfoBox').modal('hide');
                        
                    } else {
                        swal({
                            title: 'Gagal',
                            text: response.message,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        title: 'Gagal',
                        text: errorThrown,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            });
        }

        function createElementInfoBox(key,content){
            let infoBox = $('#infoBoxBtn-default').clone();
            infoBox.prop('id', 'infoBoxBtn-' + key);
            infoBox.prop('info-box-id', content.id);
            infoBox.attr('onclick', 'viewInfoBox(' + content.id + ')');
            infoBox.attr('data-bs-original-title', content.title);
            infoBox.css({left: content.pos_x + 'px', top: content.pos_y + 'px',});
            if(content.is_sold) {
                infoBox.attr('data-bs-original-title', content.tenant.name);
                infoBox.css('color','#ff3e1d');
            } else {
                infoBox.css('color','#03c3ec');
            }
            infoBox.removeClass('d-none');

            infoBox.appendTo('#mainImage');
        }

        function viewInfoBox(id, data=null) {
            // Handle modal
            $('#modalCreateInfoBox .modal-title').text('Detail Info Box');
            $('#modalCreateInfoBox form')[0].reset();
            $('#modalCreateInfoBox form [name="interactive_floor_id"]').val({{@$interactiveFloorData->id}});
            $('#modalCreateInfoBox form [name="tenant_id"]').val(0);
            $('#modalCreateInfoBox form #imgBox').attr('src','');
            $('#modalCreateInfoBox form #imgTenant').attr('src','');

            if(data) {
                $('#modalCreateInfoBox form [name="id"]').val(data.id);
                $('#modalCreateInfoBox form [name="title"]').val(data.title);
                $('#modalCreateInfoBox form [name="description"]').val(data.description);
                $('#modalCreateInfoBox form [name="pos_x"]').val(data.pos_x);
                $('#modalCreateInfoBox form [name="pos_y"]').val(data.pos_y);
                $('#modalCreateInfoBox form [name="size"]').val(data.size);
                $('#modalCreateInfoBox form [name="price"]').val(data.price);
                $('#modalCreateInfoBox form [name="external_link"]').val(data.external_link);
                $('#modalCreateInfoBox form #imgBox').attr('src',"{{url('/')}}/"+data.images);
                if(data.is_sold) {
                    $('#modalCreateInfoBox form [name="sold"]').prop("checked",true);
                }
                if(data.tenant) {
                    $('#modalCreateInfoBox form [name="tenant_id"]').val(data.tenant.id);
                    $('#modalCreateInfoBox form [name="tenant_name"]').val(data.tenant.name);
                    $('#modalCreateInfoBox form [name="tenant_description"]').val(data.tenant.description);
                    $('#modalCreateInfoBox form #imgTenant').attr('src',"{{url('/')}}/"+data.tenant.logo);
                    $('#modalCreateInfoBox form [name="tenant_ext_link"]').val(data.tenant.external_link);
                }
            } else {
                let url = "{{route('interactiveFloor.detail.infoBox',['projectId'=>$projectId,'interactiveFloorId'=>@$interactiveFloorData->id,'infoBoxId'=>':id'])}}";
                url = url.replace(':id',id);
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(response) {
                        $('#modalCreateInfoBox form [name="id"]').val(response.id);
                        $('#modalCreateInfoBox form [name="title"]').val(response.title);
                        $('#modalCreateInfoBox form [name="description"]').val(response.description);
                        $('#modalCreateInfoBox form [name="pos_x"]').val(response.pos_x);
                        $('#modalCreateInfoBox form [name="pos_y"]').val(response.pos_y);
                        $('#modalCreateInfoBox form [name="price"]').val(response.price);
                        $('#modalCreateInfoBox form [name="size"]').val(response.size);
                        $('#modalCreateInfoBox form [name="external_link"]').val(response.external_link);
                        $('#modalCreateInfoBox form #imgBox').attr('src',"{{url('/')}}/"+response.images);
                        if(response.is_sold) {
                            $('#modalCreateInfoBox form [name="sold"]').prop("checked",true);
                            $('#modalCreateInfoBox form #tenantContainer').removeClass('d-none');
                        } else {
                            $('#modalCreateInfoBox form #tenantContainer').addClass('d-none');
                        }
                        if(response.tenant) {
                            $('#modalCreateInfoBox form [name="tenant_id"]').val(response.tenant.id);
                            $('#modalCreateInfoBox form [name="tenant_name"]').val(response.tenant.name);
                            $('#modalCreateInfoBox form [name="tenant_description"]').val(response.tenant.description);
                            $('#modalCreateInfoBox form #imgTenant').attr('src',"{{url('/')}}/"+response.tenant.logo);
                            $('#modalCreateInfoBox form [name="tenant_ext_link"]').val(response.tenant.external_link);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        swal({
                            title: 'System Error',
                            text: errorThrown,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                });
            }
            setSoldStatus();
            $('#modalCreateInfoBox').modal('show');
        }

        function saveInfoBox(data = null) {
            if(!data) {
                data = new FormData($('#formInfoBox')[0]);
            }
            $.ajax({
                url: "{{ route('interactiveFloor.save.infoBox',['projectId'=>$projectId,'interactiveFloorId'=>@$interactiveFloorData->id]) }}",
                type: "POST",
                data: data,
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.success) {
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            icon: 'success',
                            timer: '3000'
                        }).then(()=>{
                            $('#modalCreateInfoBox').modal('hide');
                            location.reload();
                        });
                        
                    } else {
                        swal({
                            title: 'Gagal',
                            text: response.message,
                            icon: 'error',
                            timer: '3000'
                        });
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal({
                        title: 'Gagal',
                        text: errorThrown,
                        icon: 'error',
                        timer: '3000'
                    });
                }
            });
        }

        function deleteInfoBox() {
            let id = $('#modalCreateInfoBox form [name="id"]').val();
            swal({
                title: "Yakin Hapus Info Box?",
                text: "Data akan dihapus permanen",
                icon: "warning",
                buttons: {
                    cancel: true,
                    confirm: {
                        text: 'Hapus!',
                        closeModal: false,
                    },
                },
            }).then((process) => {
                if (process) {
                    let url = "{{route('interactiveFloor.delete.infoBox',['projectId'=>$projectId,'interactiveFloorId'=>@$interactiveFloorData->id,'infoBoxId'=>':id'])}}";
                    url = url.replace(':id',id);
                    $.ajax({
                        url: url,
                        type: "DELETE",
                        data: {
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            swal({
                                title: 'Berhasil',
                                text: response.message,
                                icon: 'success',
                                timer: '3000'
                            }).then(() => {
                                location.reload();
                            });
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal({
                                title: 'System Error',
                                text: errorThrown,
                                icon: 'error',
                                timer: '3000'
                            });
                        }
                    });
                }
            })
        }

        function setSoldStatus() {
            if($('[name="sold"]').prop('checked')) {
                $('#tenantContainer').removeClass('d-none');
            } else {
                $('#tenantContainer').addClass('d-none');
            }
        }
    </script>
    @endpush
@endsection
