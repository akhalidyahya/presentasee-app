<!-- Modal -->
<div class="modal fade" id="modalCreateInfoBox" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="formInfoBox" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="">
            <input type="hidden" name="interactive_floor_id" value="">
            <input type="hidden" name="pos_x">
            <input type="hidden" name="pos_y">
            <input type="hidden" name="mainImgWidth" value="{{@$interactiveFloorData->width}}">
            <input type="hidden" name="mainImgHeight" value="{{@$interactiveFloorData->height}}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="mb-3">
                            <label for="nama" class="form-label">Judul</label>
                            <input type="text" id="nama" name="title" class="form-control"
                                placeholder="Booth 1" />
                        </div>
                        <div class="mb-3">
                            <label for="size" class="form-label">Ukuran</label>
                            <input type="text" id="size" name="size" class="form-control"
                                placeholder="2x3" />
                        </div>
                        <div class="mb-3">
                            <label for="price" class="form-label">Harga</label>
                            <input type="text" id="price" name="price" class="form-control"
                                placeholder="1.500.000" />
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Deskripsi</label>
                            <textarea name="description" class="form-control" id="description" rows="3" placeholder="Mulai dari IDR 1.000.000 dan ..."></textarea>
                        </div>
                        <div class="mb-3 row">
                            <label for="image" class="form-label">Gambar Booth</label>
                            <div class="col text-center">
                                <img id="imgBox" class="mb-1" src="" alt="" width="50%">
                                <input type="file" name="image" id="image" class="form-control">
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="external_link" class="form-label">External Link</label>
                            <input type="text" id="external_link" name="external_link" class="form-control"
                                placeholder="https://www.google.com/harga" />
                        </div>
                        <div class="mb-2">
                            <div class="form-check form-switch mb-2 text-right">
                                <input class="form-check-input" type="checkbox" id="sold" name="sold"
                                    value="1" onchange="setSoldStatus()"
                                    />
                                <label class="form-check-label" for="sold"><strong>Sudah terjual</strong></label>
                            </div>
                        </div>
                        <input type="hidden" name="tenant_id" value="0">
                        <div id="tenantContainer" class="d-none">
                            <div class="mb-3">
                                <h4>Informasi Penyewa</h4>
                                <label for="tenant_name" class="form-label">Nama Penyewa</label>
                                <input type="text" id="tenant_name" name="tenant_name" class="form-control"
                                    placeholder="PT Mencari kebahagiaan" />
                            </div>
                            <div class="mb-3">
                                <label for="tenant_description" class="form-label">Deskripsi Penyewa</label>
                                <textarea type="text" id="tenant_description" name="tenant_description" class="form-control"
                                    placeholder="PT Mencari kebahagiaan merupakan perusahaan yang" ></textarea>
                            </div>
                            <div class="mb-3 row">
                                <label for="tenant_image" class="form-label">Logo Penyewa</label>
                                <div class="col text-center">
                                    <img id="imgTenant" class="mb-1" src="" alt="" width="50%">
                                    <input type="file" name="tenant_image" id="tenant_image" class="form-control">
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="tenant_ext_link" class="form-label">Eksternal Link Penyewa</label>
                                <input type="text" id="tenant_ext_link" name="tenant_ext_link" class="form-control"
                                    placeholder="https://mencarikebahagiaan.com/" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger float-start" onclick="deleteInfoBox()">Hapus</button>
                    {{-- <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button> --}}
                    <button type="button" class="btn btn-primary" onclick="saveInfoBox()">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
