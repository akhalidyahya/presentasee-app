<!-- Modal -->
<div class="modal fade" id="modalShareFloor" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form id="formProject" method="POST">
            @csrf
            <input type="hidden" name="projectId" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="nav-align-top mb-4">
                                <ul class="nav nav-tabs nav-fill" role="tablist">
                                    <li class="nav-item">
                                        <button type="button" class="nav-link active" role="tab"
                                            data-bs-toggle="tab" data-bs-target="#navs-justified-profile"
                                            aria-controls="navs-justified-profile" aria-selected="true">
                                            <i class="tf-icons bx bx-link"></i> Link
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                            data-bs-target="#navs-justified-messages"
                                            aria-controls="navs-justified-messages" aria-selected="false">
                                            <i class="tf-icons bx bx-message-square"></i> iFrame
                                        </button>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active show" id="navs-justified-profile" role="tabpanel">
                                        <input type="text" name="share_link" class="form-control" value="">
                                    </div>
                                    <div class="tab-pane fade" id="navs-justified-messages" role="tabpanel">
                                        <textarea name="share_iframe" class="form-control" rows="3"> </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button>
                    {{-- <button type="button" class="btn btn-primary" onclick="saveProject()">Simpan</button> --}}
                </div>
            </div>
        </form>
    </div>
</div>
