<!-- Modal -->
<div class="modal fade" id="modalCreateProject" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form id="formProject" method="POST">
            @csrf
            <input type="hidden" name="projectId" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="mb-3 row">
                            <label for="logo" class="form-label">Logo</label>
                            <div class="col text-center">
                                <img src="" alt="" width="100px">
                                <input type="file" name="logo" id="logo" class="form-control">
                                <div id="error-logo" class="error text-danger"></div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="nama" class="form-label">Nama Event</label>
                            <input type="text" id="nama" name="title" class="form-control"
                                placeholder="Gebyar Expo 2022" />
                            <div id="error-title" class="error text-danger"></div>
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Deskripsi</label>
                            <textarea name="description" class="form-control" id="description" rows="3" placeholder="Acara ini merupakan..."></textarea>
                            <div id="error-description" class="error text-danger"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Tutup
                    </button>
                    <button type="button" class="btn btn-primary" onclick="saveProject()">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
