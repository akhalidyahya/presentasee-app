@extends('layouts.base')
@section('title', 'Interactive Floor')
@push('customCSS')
    <!-- Datatable css -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/responsive.bootstrap5.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/datatables.checkboxes.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables-bs5/buttons.bootstrap5.css') }}">
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4">
            <span class="muted fw-light">Interactive Floor/</span> Daftar Interactive Floor
        </h4>
        <div class="row">
            <div class="col-lg-12 mb-4 order-0">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-primary">Daftar Interactive Floor</h5>
                        <div class="card-datatable table-responsive text-nowrap pt-0">
                            <table class="datatables-basic table border-top">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date</th>
                                        <th>Salary</th>
                                        <th>Status</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('customJS')
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.responsive.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/responsive.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables.checkboxes.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/datatables-buttons.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.bootstrap5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.html5.js') }}"></script>
        <script src="{{ asset('assets/vendor/libs/datatables-bs5/js/buttons.print.js') }}"></script>
        {{-- <script src="{{ asset('assets/js/docs-tables-datatables.js') }}"></script> --}}
        <script>
            $(function() {

                'use strict';

                var dt_basic_table = $('.datatables-basic');
                dt_basic_table.DataTable({
                    'processing': true,
                    'serverSide': true,
                    'language': {
                        processing: '<i class="fa fa-spinner fa-spin fa-2x fa-fw" style="color:#07a7e1"></i><span class="sr-only">Loading...</span> '
                    },
                    'ajax': {
                        url: "{{asset('assets/json/table-datatable.json')}}",
                    },
                    'dataType': 'json',
                    'searching': true,
                    'paging': true,
                    'lengthChange': true,
                    'columns': [
                        { data: 'id', name: 'id'},
                        { data: 'full_name', name: 'full_name'},
                        { data: 'email', name: 'email'},
                        { data: 'start_date', name: 'start_date'},
                        { data: 'salary', name: 'salary'},
                        { data: 'status', name: 'status'},
                        // { data: '', name: ''},
                    ],
                    'info': true,
                    'autoWidth': false
                });
            });
        </script>
    @endpush
@endsection
