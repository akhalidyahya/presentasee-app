<?php

use App\Http\Controllers\Controller\AuthController;
use App\Http\Controllers\Controller\InteractiveFloorController;
use App\Http\Controllers\Controller\ProjectController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\View\InteractiveFloorController as ViewInteractiveFloorController;
use App\Http\Controllers\View\PageController;
use App\Http\Controllers\View\ProjectController as ViewProjectController;
use App\Http\Middleware\XFrameOptions;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home page
Route::get('/', [PageController::class,'home'])->name('home.index');
Route::get('/join', [ProjectController::class,'join'])->name('join');

// Auth page
Route::get('/register', [PageController::class,'register'])->name('register.index');
Route::get('/login', [PageController::class,'login'])->name('login.index');
Route::post('/register', [AuthController::class,'register'])->name('register.process');
Route::post('/login', [AuthController::class,'login'])->name('login.process');
Route::post('/logout',[AuthController::class,'logout'])->name('logout');

// Present page
Route::get('events/{eventCode}/present/{floorId?}',[ViewProjectController::class,'present'])
    ->name('interactiveFloor.present');
Route::get('events/{eventCode}/iframe/{floorId?}',[ViewProjectController::class,'iframe'])
    ->name('interactiveFloor.iframe')
    ->middleware(XFrameOptions::class);
Route::post('guestBook/{projectId}',[ProjectController::class,'saveGuestBook'])->name('guestBook');

Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard.index')->middleware('auth');

Route::prefix('interactive-floor')->name('interactiveFloor.')->middleware(['auth'])->group(function(){
    // Project
    Route::get('events',[ViewProjectController::class,'list'])->name('view.project.list');
    Route::get('events/{projectId}',[ViewProjectController::class,'detail'])->name('view.project.detail');
    Route::get('events/{projectId}/json',[ProjectController::class,'getProject'])->name('project.getProject');
    Route::get('events/{projectId}/present',[ViewProjectController::class,'present'])->name('project.present');
    Route::post('events',[ProjectController::class,'saveProject'])->name('project.saveProject');
    Route::delete('events/{projectId}',[ProjectController::class,'deleteProject'])->name('project.delete');
    Route::post('events/{projectId}/status/{status}',[ProjectController::class,'setStatus'])->name('project.setStatus');

    // Interactive Floor
    Route::get('events/{projectId}/interactiveFloor/create',[ViewInteractiveFloorController::class,'create'])->name('create');
    Route::get('events/{projectId}/interactiveFloor/{interactiveFloorId}',[ViewInteractiveFloorController::class,'detail'])->name('detail');
    Route::post('events/{projectId}/interactiveFloor',[InteractiveFloorController::class,'saveInteractiveFloor'])->name('save');
    Route::delete('events/{projectId}/interactiveFloor/{interactiveFloorId}',[InteractiveFloorController::class,'deleteInteractiveFloor'])->name('delete');

    // Info Box
    Route::post('events/{projectId}/interactiveFloor/{interactiveFloorId?}/saveInfoBox',[InteractiveFloorController::class,'saveInteractiveFloorInfoBox'])->name('save.infoBox');
    Route::get('events/{projectId}/interactiveFloor/{interactiveFloorId?}/infoBox/{infoBoxId?}',[InteractiveFloorController::class,'getInteractiveFloorInfoBox'])->name('detail.infoBox');
    Route::delete('events/{projectId}/interactiveFloor/{interactiveFloorId?}/infoBox/{infoBoxId?}',[InteractiveFloorController::class,'deleteInteractiveFloorInfoBox'])->name('delete.infoBox');
    Route::get('events/{projectId}/interactiveFloorDataTable',[InteractiveFloorController::class,'getDataTable'])->name('getDataTable');
});


// Helper 
Route::get('/sessionFlush', function(){
    session()->flush();
});