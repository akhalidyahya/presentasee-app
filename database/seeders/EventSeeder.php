<?php

namespace Database\Seeders;

use App\Models\InteractiveFloor;
use App\Models\Project;
use App\Util\Constant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create event
        Project::create([
            'created_by' => 2,
            'code' => strtolower(Str::random(5)),
            'title' => 'International Hacker Expo 2022',
            'description' => 'An exhibition, in the most general sense, is an organized',
            'type' => Constant::PROJECT_TYPE_EVENT,
            'status' => Constant::PUBLISH_STATUS_UNPUBLISH,
            'remark' => '',
        ]);

        // Create Interactive Floor
        InteractiveFloor::create([
            'project_id' => 1,
            'title' => 'Lantai 1',
            'description' => 'Lantai 1 berada di...',
            'main_image' => 'public/storage/floor/mla-2019-exhibit-hall.png',
            'width' => NULL,
            'height' => NULL,
            'status' => Constant::PUBLISH_STATUS_UNPUBLISH,
            'remark' => '',
        ]);
    }
}
