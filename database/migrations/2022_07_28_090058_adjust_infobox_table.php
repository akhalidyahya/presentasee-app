<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdjustInfoboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interactive_floor_info_boxes', function (Blueprint $table) {
            $table->string('size')->after('images')->nullable();
            $table->string('price')->after('images')->nullable();
            $table->string('external_link')->after('images')->nullable();
            $table->string('status')->after('images')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interactive_floor_infobox', function (Blueprint $table) {
            //
        });
    }
}
