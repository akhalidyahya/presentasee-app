<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteractiveFloorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interactive_floors', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->unsigned()->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('main_image')->nullable();
            $table->float('width')->nullable();
            $table->float('height')->nullable();
            $table->string('status')->index();
            $table->text('remark')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interactive_floors');
    }
}
