<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteractiveFloorInfoBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interactive_floor_info_boxes', function (Blueprint $table) {
            $table->id();
            $table->integer('interactive_floor_id')->unsigned()->index();
            $table->float('pos_x')->nullable();
            $table->float('pos_y')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->longText('images')->nullable();
            $table->text('remark')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interactive_floor_info_boxes');
    }
}
