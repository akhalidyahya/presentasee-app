<?php

namespace App\Http\Controllers\Controller;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Util\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->except(['_token','captcha','remember_me']);

        $user = User::where('email',$validatedData['email'])->first();

        if (Auth::attempt($credentials,$request->remember_me)) {
            $request->session()->regenerate();

            // Email verified validation (not used yet)
            // if(Auth::user()->email_verified_at == NULL && Auth::user()->role == Constants::ROLE_USER) {
            //     Auth::logout();
            //     session()->flash('message', 'Email belum diverifikasi!');
            //     return redirect()->back();
            // }
            
            return redirect()->route('dashboard.index');

        }else{
            session()->flash('message',['status'=>'danger','message'=>'Email atau Password yang anda masukkan salah!']);
            return redirect()->back();
        }
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ];
 
        $messages = [
            'email.unique'  => 'Email sudah terdaftar.',
        ];
 
        $validatedData = $request->validate($rules,$messages);

        $user = new User();
        $user->name                  = $validatedData['name'];
        $user->email                 = $validatedData['email'];
        $user->password              = bcrypt($validatedData['password']);
        $user->role                  = Constant::USER_ROLE_USER;

        if($user->save()) {
            $message = 'Pendaftaran berhasil! Silahkan login!';
            return redirect()->route('login.index')->with('message',['status'=>'success','message'=>$message]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.index');
    }
}
