<?php

namespace App\Http\Controllers\Controller;

use App\Http\Controllers\Controller;
use App\Models\Guest;
use App\Models\Project;
use App\Util\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    public function join(Request $request) {
        $code = $request->code ?? abort(404);
        $data['event'] = Project::with('interactiveFloors.infoBox')
                            ->where('code',$code)
                            ->where('status',Constant::PUBLISH_STATUS_PUBLISHED)
                            ->first() ?? abort(404);
        return redirect()->route('interactiveFloor.present',['eventCode'=>$code]);
    }

    public function getProject(Request $request, $projectId)
    {
        $project = Project::find($projectId);
        if(empty($project)) {
            throw new Exception(__('api.not_found',['name'=>'Event']),404);
        }
        return $project;
    }

    public function saveProject(Request $request)
    {
        $project = Project::find($request->projectId);
        if(empty($project)) {
            $project = new Project();
            $project->created_by = Auth::user()->id; // TO BE REPLACED BY USER ID
            $project->code = strtolower(Str::random(5));
            $project->type = Constant::PROJECT_TYPE_EVENT;
            $project->status = Constant::PUBLISH_STATUS_UNPUBLISH;
        }
        $project->title = $request->title;
        $project->description = $request->description;
        if(!empty($request->logo)) {
            $rules = [
                'logo' => 'sometimes|mimes:jpeg,png,jpg,gif',
            ];
            $request->validate($rules);

            if(!empty($project->logo)) {
                try {
                    unlink($project->logo);
                } catch (\Throwable $th) {
                    Log::info($th);
                }
            }
            $file = $request->file('logo');
            $fileName = date('YmdHis').$file->getClientOriginalName();
            $path = $file->storeAs('public/logo',$fileName);
            $project->logo = 'public/storage/logo/'.$fileName;
        }
        if($project->save()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_saved',['name'=>'Event']),
                'data' => $project
            ],201);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Event'])
        ]);
    }

    public function deleteProject($projectId)
    {
        $project = Project::find($projectId);
        if(empty($project)) {
            throw new Exception(__('api.not_found',['name'=>'Event']),404);
        }
        
        if($project->delete()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_deleted',['name'=>'Event']),
            ],201);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Event'])
        ]);
    }

    public function setStatus($projectId,$status) {
        $project = Project::find($projectId) ?? abort(404);
        $statuses = array_keys(Constant::PUBLISH_STATUS_LIST);
        if(!in_array($status,$statuses)) abort(404);
        $project->status = $status;
        if($project->save()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_saved',['name'=>'Event']),
                'data' => $project
            ],201);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Event'])
        ]);
    }

    public function saveGuestBook(Request $request, $projectId) {
        Guest::create([
            'name' => $request->name,
            'project_id' => $projectId,
        ]);
        session(['guestName' => $request->name]);
        return response()->json(['success'=>true]);
    }
}
