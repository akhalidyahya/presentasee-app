<?php

namespace App\Http\Controllers\Controller;

use App\Http\Controllers\Controller;
use App\Models\InteractiveFloor;
use App\Models\InteractiveFloorInfoBox;
use App\Models\Tenant;
use App\Util\Constant;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class InteractiveFloorController extends Controller
{
    public function saveInteractiveFloor(Request $request,$projectId) {
        $Floor = InteractiveFloor::find($request->interactiveFloorId);
        if(empty($Floor)) {
            $Floor = new InteractiveFloor();
            $Floor->project_id = $projectId;
            $Floor->status = Constant::PUBLISH_STATUS_UNPUBLISH;
        }
        $formData = $request->only(['project_id','title','description']);
        $Floor->fill($formData);

        if($request->has('main_image') && !empty($request->main_image)) {
            $rules = [
                'main_image' => 'sometimes|mimes:jpeg,png,jpg,gif',
            ];
            $request->validate($rules);

            if(!empty($Floor->main_image)) {
                try {
                    unlink($Floor->main_image);
                } catch (\Throwable $th) {
                    Log::info($th);
                }
            }
            $file = $request->file('main_image');
            $fileName = date('YmdHis').$file->getClientOriginalName();
            $path = $file->storeAs('public/floor',$fileName);
            $Floor->main_image = 'public/storage/floor/'.$fileName;
        }

        if($Floor->save()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_saved',['name'=>'Interactive Floor']),
                'data'    => $Floor
            ],);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Interactive Floor'])
        ]);
    }

    public function deleteInteractiveFloor(Request $request, $projectId, $interactiveFloorId) {
        $data = InteractiveFloor::find($interactiveFloorId);
        if(empty($data)) {
            throw new Exception(__('api.not_found',['name'=>'Interactive Floor']),404);
        }
        
        if($data->delete()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_deleted',['name'=>'Interactive Floor']),
            ],201);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Interactive Floor'])
        ]);
    }

    public function saveInteractiveFloorInfoBox(Request $request, $projectId,$interactiveFloorId) {
        // test response
        // return response()->json(['data'=>$request->all()]);
        $InfoBox = InteractiveFloorInfoBox::find($request->id);
        if(empty($InfoBox)) {
            $InfoBox = new InteractiveFloorInfoBox();
            $InfoBox->interactive_floor_id = $interactiveFloorId;
        }
        $formData = $request->only(['pos_x','pos_y','title','size','price','description','remark','external_link']);
        $InfoBox->fill($formData);

        if(!empty($request->image)) {
            $rules = [
                'images' => 'sometimes|mimes:jpeg,png,jpg,gif',
            ];
            $request->validate($rules);

            if(!empty($InfoBox->images)) {
                try {
                    unlink($InfoBox->images);
                } catch (\Throwable $th) {
                    Log::info($th);
                }
            }
            $file = $request->file('image');
            $fileName = date('YmdHis').$file->getClientOriginalName();
            $path = $file->storeAs('public/infobox',$fileName);
            $InfoBox->images = 'public/storage/infobox/'.$fileName;
        }

        $InfoBox->status = (!empty($request->sold) && @$request->sold == 1) ? Constant::BOOTH_STATUS_SOLD : Constant::BOOTH_STATUS_NEW;

        if($InfoBox->save()) {
            // DB::statement("UPDATE interactive_floors SET width = $request->width, height = $request->height WHERE id = $request->interactive_floor_id; ");
            DB::table('interactive_floors')->where('id',$request->interactive_floor_id)->update(['width'=>$request->mainImgWidth,'height'=>$request->mainImgHeight]);
            if(!empty($request->sold) && @$request->sold == 1) {
                $tenant = Tenant::find($request->tenant_id);
                if(empty($tenant)) {
                    $tenant = new Tenant();
                    $tenant->infobox_id = $InfoBox->id;
                }
                $tenant->name = $request->tenant_name;
                $tenant->description = $request->tenant_description;
                $tenant->external_link = $request->tenant_ext_link;
                if(!empty($request->tenant_image)) {
                    $rules = [
                        'tenant_image' => 'sometimes|mimes:jpeg,png,jpg,gif',
                    ];
                    $request->validate($rules);
        
                    if(!empty($tenant->tenant_image)) {
                        try {
                            unlink($tenant->tenant_image);
                        } catch (\Throwable $th) {
                            Log::info($th);
                        }
                    }
                    $file = $request->file('tenant_image');
                    $fileName = date('YmdHis').$file->getClientOriginalName();
                    $path = $file->storeAs('public/tenant',$fileName);
                    $tenant->logo = 'public/storage/tenant/'.$fileName;
                }
                $tenant->save();
            }
            return response()->json([
                'success' => true,
                'message' => __('api.data_saved',['name'=>'Interactive Floor Info Box']),
                'data'    => $InfoBox
            ],);
        }

        return response()->json([
            'success' => false,
            'message' => __('api.data_not_processed',['name'=>'Interactive Floor Info Box'])
        ]);
    }

    public function getInteractiveFloorInfoBox($projectId,$interactiveFloorId,$infoBoxId) {
        $data = InteractiveFloorInfoBox::with('tenant')->find($infoBoxId);
        if(empty($data)) {
            throw new Exception(__('api.not_found',['name'=>'Info Box']),404);
        }
        return $data;
    }

    public function deleteInteractiveFloorInfoBox($projectId,$interactiveFloorId,$infoBoxId) {
        $data = InteractiveFloorInfoBox::find($infoBoxId);
        if(empty($data)) {
            throw new Exception(__('api.not_found',['name'=>'Info Box']),404);
        }
        if($data->delete()) {
            return response()->json([
                'success' => true,
                'message' => __('api.data_saved',['name'=>'Info Box']),
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => __('api.data_not_processed',['name'=>'Info Box'])
            ]);
        }
    }

    public function getDataTable(Request $request, $projectId) {
        $datas = InteractiveFloor::with('project')->where('project_id',$projectId)->get();
        return DataTables::of($datas)
            ->addIndexColumn()
            ->addColumn('action',function($data){
                return '<a href="'.route('interactiveFloor.detail',['projectId'=>$data->project_id,'interactiveFloorId'=>$data->id]).'" class="btn btn-sm text-info btn-icon item-edit"><i class="bx bxs-pencil"></i></a>'.
                '<a href="javascript:;" onclick="deleteFloor('.$data->id.')" class="btn btn-sm text-danger btn-icon item-edit"><i class="bx bxs-trash"></i></a>'.
                '<a href="javascript:;" onclick="shareFloor(
                    \''.$data->title.'\',
                    \''.route('interactiveFloor.present', ['eventCode' => $data->project->code,'floorId'=>$data->id]).'\',
                    \''.route('interactiveFloor.iframe', ['eventCode' => $data->project->code,'floorId'=>$data->id]).'\',
                )" class="btn btn-sm text-info btn-icon item-edit"><i class="bx bxs-share"></i></a>';
            })
            ->escapeColumns([])->make(true);
    }
}
