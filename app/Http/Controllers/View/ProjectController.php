<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Util\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function list()
    {
        $data['sidebar'] = 'projectList';
        $data['projectData'] = Project::where('created_by',Auth::user()->id)->get();
        return view('pages.interactive-floor.project-list',$data);
    }

    public function detail(Request $request, $projectId)
    {
        $data['sidebar'] = 'projectList';
        $data['projectData'] = Project::find($projectId) ?? abort(404);
        return view('pages.interactive-floor.project-detail',$data);
    }

    public function present($eventCode,$floorId=NULL)
    {
        $data = [];
        $data['isIframe'] = false;
        $data['event'] = Project::with('interactiveFloors.infoBox.tenant')
                            ->where('code',$eventCode)
                            ->where('status',Constant::PUBLISH_STATUS_PUBLISHED)
                            ->first() ?? abort(404);
        if(!empty($floorId)) {
            $data['floor'] = collect($data['event']->interactiveFloors)->where('id',$floorId)->flatten()->first();
            return view('pages.frontend.interactiveFloor-present',$data);
        }

        return view('pages.frontend.interactiveFloor-presentHome',$data);
    }

    // Not a good way. Duplicate code. Will fix later
    public function iframe($eventCode,$floorId=NULL)
    {
        $data = [];
        $data['isIframe'] = true;
        $data['event'] = Project::with('interactiveFloors.infoBox')
                            ->where('code',$eventCode)
                            ->where('status',Constant::PUBLISH_STATUS_PUBLISHED)
                            ->first() ?? abort(404);
        if(!empty($floorId)) {
            $data['floor'] = collect($data['event']->interactiveFloors)->where('id',$floorId)->flatten()->first();
            return view('pages.frontend.interactiveFloor-present',$data);
        }

        return view('pages.frontend.interactiveFloor-presentHome',$data);
    }
}
