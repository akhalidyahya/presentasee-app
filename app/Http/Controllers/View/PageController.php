<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home() {
        $data = [];
        return view('pages.frontend.home',$data);
    }

    public function register() {
        $data = [];
        return view('pages.frontend.register',$data);
    }
    
    public function login() {
        $data = [];
        return view('pages.frontend.login',$data);
    }
}
