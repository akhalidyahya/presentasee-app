<?php

namespace App\Http\Controllers\view;

use App\Http\Controllers\Controller;
use App\Models\InteractiveFloor;
use App\Models\Project;
use Illuminate\Http\Request;

class InteractiveFloorController extends Controller
{
    public function create($projectId) 
    {
        $data['sidebar'] = 'projectList';
        $data['projectId'] = $projectId;
        $data['project'] = Project::find($projectId);
        $data['interactiveFloorData'] = InteractiveFloor::class;
        return view('pages.interactive-floor.main-form',$data);
    }
    public function detail($projectId,$interactiveFloorId)
    {
        $data['sidebar'] = 'projectList';
        $data['projectId'] = $projectId;
        $data['interactiveFloorData'] = InteractiveFloor::with(['project','infoBox.tenant'])->where('id',$interactiveFloorId)->first() ?? abort('404');
        return view('pages.interactive-floor.main-form',$data);
    }
}
