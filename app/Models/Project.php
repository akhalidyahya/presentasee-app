<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'created_by',
        'code',
        'logo',
        'type',
        'status',
        'title',
        'description',
    ];

    protected $appends = [
        'creator'
    ];

    public function getCreatorAttribute()
    {
        return User::find(@$this->attributes['created_by']);
    }

    /**
     * Get all of the interactiveFloor for the Project
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interactiveFloors(): HasMany
    {
        return $this->hasMany(InteractiveFloor::class, 'project_id', 'id');
    }
}
