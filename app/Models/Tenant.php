<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tenant extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'infobox_id',
        'name',
        'description',
        'logo',
        'external_link',
    ];

    /**
     * Get the infoBox that owns the Tenant
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function infoBox(): BelongsTo
    {
        return $this->belongsTo(InteractiveFloorInfoBox::class, 'infobox_id', 'id');
    }
}
