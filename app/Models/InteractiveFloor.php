<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class InteractiveFloor extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'project_id',
        'title',
        'description',
        'main_image',
        'width',
        'height',
        'status',
        'remark',
    ];

    /**
     * Get the Project that owns the InteractiveFloor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
     * Get all of the interactiveFloorInfoBox for the InteractiveFloor
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function infoBox(): HasMany
    {
        return $this->hasMany(InteractiveFloorInfoBox::class, 'interactive_floor_id', 'id');
    }
}
