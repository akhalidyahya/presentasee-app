<?php

namespace App\Models;

use App\Util\Constant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class InteractiveFloorInfoBox extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'interactive_floor_id',
        'pos_x',
        'pos_y',
        'title',
        'description',
        'remark',
        'images',
        'size',
        'price',
        'external_link',
        'status',
    ];

    protected $appends = [
        'is_sold'
    ];

    public function getIsSoldAttribute()
    {
        return $this->attributes['status'] == Constant::BOOTH_STATUS_SOLD ? true : false;
    }

    /**
     * Get the user that owns the InteractiveFloorInfoBox
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function interactiveFloor(): BelongsTo
    {
        return $this->belongsTo(InteractiveFloor::class, 'interactive_floor_id', 'id');
    }

    /**
     * Get the tenant associated with the InteractiveFloorInfoBox
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function tenant(): HasOne
    {
        return $this->hasOne(Tenant::class, 'infobox_id', 'id');
    }
}
