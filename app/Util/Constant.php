<?php
namespace App\Util;

class Constant {
    const USER_ROLE_USER = 'USER';
    const USER_ROLE_ADMIN = 'ADMIN';

    const PUBLISH_STATUS_PUBLISHED = 'PUBLISHED';
    const PUBLISH_STATUS_UNPUBLISH = 'UNPUBLISH';
    const PUBLISH_STATUS_LIST = [
        self::PUBLISH_STATUS_PUBLISHED => 'Tayang',
        self::PUBLISH_STATUS_UNPUBLISH => 'Tidak Tayang',
    ];

    const PROJECT_TYPE_EVENT = 'EVENT';

    const BOOTH_STATUS_SOLD = 'SOLD';
    const BOOTH_STATUS_NEW = 'NEW';
}

?>